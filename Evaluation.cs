using System;
using System.Collections.Generic;
using TeamHeroCoderLibrary;

public static class Evaluation
{
    public static bool HeroHasStatusEffect(StatusEffect statusEffect, Hero hero)
    {
        foreach (StatusEffectAndDuration se in hero.statusEffectsAndDurations)
        {
            if (se.statusEffect == statusEffect)
            {
                return true;
            }
        }

        return false;
    }

    public static List<Hero> FindHeroesWithStatusEffect(StatusEffect statusEffectID, List<Hero> teamOfHeroes)
    {
        List<Hero> heroesWithStatusEffect = new List<Hero>();

        foreach (Hero hero in teamOfHeroes)
        {
            if (HeroHasStatusEffect(statusEffectID, hero))
                heroesWithStatusEffect.Add(hero);
        }

        return heroesWithStatusEffect;
    }

    public static List<Hero> FindHeroesWithoutStatusEffect(StatusEffect statusEffect, List<Hero> teamOfHeroes)
    {
        List<Hero> heroesWithStatusEffect = new List<Hero>();

        foreach (Hero hero in teamOfHeroes)
        {
            if (!HeroHasStatusEffect(statusEffect, hero))
                heroesWithStatusEffect.Add(hero);
        }

        return heroesWithStatusEffect;
    }

    public static List<Hero> FindStandingHeroesWithHealthLessThanAmount(float amount, List<Hero> teamOfHeroes)
    {
        List<Hero> heroesWithHealthLessThanAmount = new List<Hero>();

        foreach (Hero hero in teamOfHeroes)
        {
            if (hero.health <= 0)
                continue;

            float p = (float)hero.health / (float)hero.maxHealth;
            if (p < amount)
                heroesWithHealthLessThanAmount.Add(hero);
        }

        return heroesWithHealthLessThanAmount;
    }

    public static List<Hero> FindStandingHeroesWithManaLessThanAmount(float amount, List<Hero> teamOfHeroes)
    {
        List<Hero> heroesWithManaLessThanAmount = new List<Hero>();

        foreach (Hero hero in teamOfHeroes)
        {
            if (hero.health <= 0)
                continue;

            float p = (float)hero.mana / (float)hero.maxMana;
            if (p < amount)
                heroesWithManaLessThanAmount.Add(hero);
        }

        return heroesWithManaLessThanAmount;
    }

    public static Hero FindStandingHeroWithLowestHealth(List<Hero> teamOfHeroes)
    {
        Hero lowestHealth = null;

        foreach (Hero hero in teamOfHeroes)
        {
            if (hero.health <= 0)
                continue;

            if (lowestHealth == null)
                lowestHealth = hero;
            else if (hero.health < lowestHealth.health)
                lowestHealth = hero;
        }

        return lowestHealth;
    }

    public static int CountNumberOfStandingHeroes(List<Hero> teamOfHeroes)
    {
        int count = 0;

        foreach (Hero hero in teamOfHeroes)
        {
            if (hero.health > 0)
                count++;
        }

        return count;
    }

    public static List<Hero> FindSlainHeroes(List<Hero> teamOfHeroes)
    {
        List<Hero> heroesWithHealthLessThanAmount = new List<Hero>();

        foreach (Hero hero in teamOfHeroes)
        {
            if (hero.health <= 0)
                heroesWithHealthLessThanAmount.Add(hero);
        }

        return heroesWithHealthLessThanAmount;
    }

    public static List<Hero> FindHeroesWithCharacterClass(HeroJobClass jobClass, List<Hero> teamOfHeroes)
    {
        List<Hero> heroesWithCharacterClass = new List<Hero>();

        foreach (Hero hero in teamOfHeroes)
        {
            if (hero.jobClass == jobClass)
                heroesWithCharacterClass.Add(hero);
        }

        return heroesWithCharacterClass;
    }

    private static List<StatusEffect> NegativeStatusEffects = new List<StatusEffect>(){
        StatusEffect.Debrave,
        StatusEffect.Defaith,
        StatusEffect.Doom,
        StatusEffect.Petrified,
        StatusEffect.Slow,
        StatusEffect.Silence
    };

    private static List<StatusEffect> PositiveStatusEffects = new List<StatusEffect>(){
        StatusEffect.Brave,
        StatusEffect.Faith,
        StatusEffect.AutoLife,
        StatusEffect.Haste,
    };

    public static int CountNumberOfNegativeStatusEffects(Hero hero)
    {
        int count = 0;
        foreach (StatusEffectAndDuration se in hero.statusEffectsAndDurations)
        {
            if (NegativeStatusEffects.Contains(se.statusEffect))
                count++;
        }

        return count;
    }

    public static int CountNumberOfPositiveStatusEffects(Hero hero)
    {
        int count = 0;
        foreach (StatusEffectAndDuration se in hero.statusEffectsAndDurations)
        {
            if (PositiveStatusEffects.Contains(se.statusEffect))
                count++;
        }

        return count;
    }

    public static List<Hero> FindHeroesWithNegativeStatusEffectsMoreThanAmount(int amount, List<Hero> teamOfHeroes)
    {
        List<Hero> heroesWithNegStatusEffectMoreThan = new List<Hero>();
        foreach (Hero hero in teamOfHeroes)
        {
            if (CountNumberOfNegativeStatusEffects(hero) > amount)
                heroesWithNegStatusEffectMoreThan.Add(hero);
        }

        return heroesWithNegStatusEffectMoreThan;
    }

    public static List<Hero> FindHeroesWithPositiveStatusEffectsMoreThanAmount(int amount, List<Hero> teamOfHeroes)
    {
        List<Hero> heroesWithPosStatusEffectMoreThan = new List<Hero>();
        foreach (Hero hero in teamOfHeroes)
        {
            if (CountNumberOfPositiveStatusEffects(hero) > amount)
                heroesWithPosStatusEffectMoreThan.Add(hero);
        }

        return heroesWithPosStatusEffectMoreThan;
    }

    public static List<Hero> FilterOutHeroesThatAreProtectedByCover(List<Hero> teamOfHeroes)
    {
        bool teamHasStandingHeroWithCover = false;

        List<Hero> heroesNotProtectedByCover = new List<Hero>();

        foreach (Hero hero in teamOfHeroes)
        {
            if (hero.health > 0 && hero.passiveAbilities.Contains(PassiveAbility.Cover))
                teamHasStandingHeroWithCover = true;
        }

        foreach (Hero hero in teamOfHeroes)
        {
            bool heroCanBeProtectedByCover = (hero.jobClass == HeroJobClass.Cleric || hero.jobClass == HeroJobClass.Wizard);
            if (teamHasStandingHeroWithCover && heroCanBeProtectedByCover)
                continue;
            else
                heroesNotProtectedByCover.Add(hero);
        }

        return heroesNotProtectedByCover;
    }

}
