using System;
using System.Collections.Generic;
using TeamHeroCoderLibrary;

public static class MyAI
{
    public static string FolderExchangePath = "Replace this text with the Team Hero Coder exchange directory";

    static bool hasPerformedAction;

    public static void ProcessAI()
    {
        hasPerformedAction = false;

        switch (TeamHeroCoder.BattleState.heroWithInitiative.jobClass)
        {
            case HeroJobClass.Fighter:
                ProcessFighterAI();
                break;

            case HeroJobClass.Cleric:
                ProcessClericAI();
                break;

            case HeroJobClass.Wizard:
                ProcessWizardAI();
                break;

            default:
                Console.WriteLine("Hero character class not found!");
                break;
        }

        #region If no action has been performed, default to attack foe with lowest health

        if (!hasPerformedAction)
        {
            Console.WriteLine("No action was found, performing default attack vs lowest health foe");
            List<Hero> heroesNotProtectedByCover = Evaluation.FilterOutHeroesThatAreProtectedByCover(TeamHeroCoder.BattleState.foeHeroes);
            Hero targetWithLowestHealthThatIsNotProtectedByCover = Evaluation.FindStandingHeroWithLowestHealth(heroesNotProtectedByCover);
            TeamHeroCoder.PerformHeroAbility(Ability.Attack, targetWithLowestHealthThatIsNotProtectedByCover);
        }

        #endregion

    }

    static private void CheckIfAbilityAndTargetAreLegalAndPerformIfSo(Ability ability, Hero target = null)
    {
        if (!hasPerformedAction && Utility.AreAbilityAndTargetLegal(ability, target, false))
        {
            TeamHeroCoder.PerformHeroAbility(ability, target);
            hasPerformedAction = true;
        }
    }

    static private void ProcessFighterAI()
    {
        Console.WriteLine("Processing AI for fighter");

        Hero fighterWithInitiative = TeamHeroCoder.BattleState.heroWithInitiative;
        List<Hero> tempHeroList;
        tempHeroList = Evaluation.FilterOutHeroesThatAreProtectedByCover(TeamHeroCoder.BattleState.foeHeroes);
        Hero targetWithLowestHealthThatIsNotProtectedByCover = Evaluation.FindStandingHeroWithLowestHealth(tempHeroList);

        #region Ether Usage

        tempHeroList = Evaluation.FindStandingHeroesWithManaLessThanAmount(0.3f, TeamHeroCoder.BattleState.allyHeroes);
        tempHeroList = Evaluation.FindHeroesWithCharacterClass(HeroJobClass.Cleric, tempHeroList);
        if (tempHeroList.Count > 0)
            CheckIfAbilityAndTargetAreLegalAndPerformIfSo(Ability.Ether, tempHeroList[0]);

        tempHeroList = Evaluation.FindStandingHeroesWithManaLessThanAmount(0.3f, TeamHeroCoder.BattleState.allyHeroes);
        tempHeroList = Evaluation.FindHeroesWithCharacterClass(HeroJobClass.Wizard, tempHeroList);
        if (tempHeroList.Count > 0)
            CheckIfAbilityAndTargetAreLegalAndPerformIfSo(Ability.Ether, tempHeroList[0]);

        #endregion

        #region Silence Rememdy Usage

        tempHeroList = Evaluation.FindHeroesWithStatusEffect(StatusEffect.Silence, TeamHeroCoder.BattleState.allyHeroes);
        tempHeroList = Evaluation.FindHeroesWithCharacterClass(HeroJobClass.Cleric, tempHeroList);
        if (tempHeroList.Count > 0)
            CheckIfAbilityAndTargetAreLegalAndPerformIfSo(Ability.SilenceRemedy, tempHeroList[0]);

        tempHeroList = Evaluation.FindHeroesWithStatusEffect(StatusEffect.Silence, TeamHeroCoder.BattleState.allyHeroes);
        tempHeroList = Evaluation.FindHeroesWithCharacterClass(HeroJobClass.Wizard, tempHeroList);
        if (tempHeroList.Count > 0)
            CheckIfAbilityAndTargetAreLegalAndPerformIfSo(Ability.SilenceRemedy, tempHeroList[0]);

        #endregion

        bool hasBraveSE = Evaluation.HeroHasStatusEffect(StatusEffect.Brave, fighterWithInitiative);
        if (!hasBraveSE)
            CheckIfAbilityAndTargetAreLegalAndPerformIfSo(Ability.Brave, fighterWithInitiative);

        if (hasBraveSE)
            CheckIfAbilityAndTargetAreLegalAndPerformIfSo(Ability.QuickHit, targetWithLowestHealthThatIsNotProtectedByCover);

        CheckIfAbilityAndTargetAreLegalAndPerformIfSo(Ability.Attack, targetWithLowestHealthThatIsNotProtectedByCover);

    }

    static private void ProcessClericAI()
    {
        Console.WriteLine("Processing AI for cleric");

        List<Hero> slainAllyHeroes = Evaluation.FindSlainHeroes(TeamHeroCoder.BattleState.allyHeroes);
        List<Hero> allyHeroesWithHealthLessThan30Percent = Evaluation.FindStandingHeroesWithHealthLessThanAmount(0.3f, TeamHeroCoder.BattleState.allyHeroes);
        List<Hero> allyHeroesWithHealthLessThan50Percent = Evaluation.FindStandingHeroesWithHealthLessThanAmount(0.5f, TeamHeroCoder.BattleState.allyHeroes);
        List<Hero> allyHeroesWithHealthLessThan70Percent = Evaluation.FindStandingHeroesWithHealthLessThanAmount(0.7f, TeamHeroCoder.BattleState.allyHeroes);
        List<Hero> tempHeroList;

        #region Emergency Heals/Resurrect

        if (slainAllyHeroes.Count > 0)
            CheckIfAbilityAndTargetAreLegalAndPerformIfSo(Ability.Resurrection, slainAllyHeroes[0]);

        if (allyHeroesWithHealthLessThan30Percent.Count >= 2)
            CheckIfAbilityAndTargetAreLegalAndPerformIfSo(Ability.MassHeal, allyHeroesWithHealthLessThan30Percent[0]);

        if (allyHeroesWithHealthLessThan30Percent.Count > 0)
            CheckIfAbilityAndTargetAreLegalAndPerformIfSo(Ability.CureSerious, allyHeroesWithHealthLessThan30Percent[0]);

        #endregion

        #region Quick Cleanse

        tempHeroList = Evaluation.FindHeroesWithNegativeStatusEffectsMoreThanAmount(1, TeamHeroCoder.BattleState.allyHeroes);
        if (tempHeroList.Count > 0)
            CheckIfAbilityAndTargetAreLegalAndPerformIfSo(Ability.QuickCleanse, tempHeroList[0]);

        tempHeroList = Evaluation.FindHeroesWithStatusEffect(StatusEffect.Slow, TeamHeroCoder.BattleState.allyHeroes);
        if (tempHeroList.Count > 0)
            CheckIfAbilityAndTargetAreLegalAndPerformIfSo(Ability.QuickCleanse, tempHeroList[0]);

        tempHeroList = Evaluation.FindHeroesWithStatusEffect(StatusEffect.Petrified, TeamHeroCoder.BattleState.allyHeroes);
        if (tempHeroList.Count > 0)
            CheckIfAbilityAndTargetAreLegalAndPerformIfSo(Ability.QuickCleanse, tempHeroList[0]);

        tempHeroList = Evaluation.FindHeroesWithStatusEffect(StatusEffect.Doom, TeamHeroCoder.BattleState.allyHeroes);
        if (tempHeroList.Count > 0)
            CheckIfAbilityAndTargetAreLegalAndPerformIfSo(Ability.QuickCleanse, tempHeroList[0]);

        tempHeroList = Evaluation.FindHeroesWithStatusEffect(StatusEffect.Petrifying, TeamHeroCoder.BattleState.allyHeroes);
        if (tempHeroList.Count > 0)
            CheckIfAbilityAndTargetAreLegalAndPerformIfSo(Ability.QuickCleanse, tempHeroList[0]);

        tempHeroList = Evaluation.FindHeroesWithStatusEffect(StatusEffect.Defaith, TeamHeroCoder.BattleState.allyHeroes);
        tempHeroList = Evaluation.FindHeroesWithCharacterClass(HeroJobClass.Wizard, tempHeroList);
        if (tempHeroList.Count > 0)
            CheckIfAbilityAndTargetAreLegalAndPerformIfSo(Ability.QuickCleanse, tempHeroList[0]);

        tempHeroList = Evaluation.FindHeroesWithStatusEffect(StatusEffect.Defaith, TeamHeroCoder.BattleState.allyHeroes);
        tempHeroList = Evaluation.FindHeroesWithCharacterClass(HeroJobClass.Cleric, tempHeroList);
        if (tempHeroList.Count > 0)
            CheckIfAbilityAndTargetAreLegalAndPerformIfSo(Ability.QuickCleanse, tempHeroList[0]);

        tempHeroList = Evaluation.FindHeroesWithStatusEffect(StatusEffect.Debrave, TeamHeroCoder.BattleState.allyHeroes);
        tempHeroList = Evaluation.FindHeroesWithCharacterClass(HeroJobClass.Fighter, tempHeroList);
        if (tempHeroList.Count > 0)
            CheckIfAbilityAndTargetAreLegalAndPerformIfSo(Ability.QuickCleanse, tempHeroList[0]);

        #endregion

        #region Mid Importance Heals

        if (allyHeroesWithHealthLessThan50Percent.Count >= 2)
            CheckIfAbilityAndTargetAreLegalAndPerformIfSo(Ability.MassHeal, allyHeroesWithHealthLessThan50Percent[0]);

        if (allyHeroesWithHealthLessThan50Percent.Count > 0)
            CheckIfAbilityAndTargetAreLegalAndPerformIfSo(Ability.QuickHeal, allyHeroesWithHealthLessThan50Percent[0]);

        #endregion

        #region Ether Usage

        tempHeroList = Evaluation.FindStandingHeroesWithManaLessThanAmount(0.3f, TeamHeroCoder.BattleState.allyHeroes);
        tempHeroList = Evaluation.FindHeroesWithCharacterClass(HeroJobClass.Cleric, tempHeroList);
        if (tempHeroList.Count > 0)
            CheckIfAbilityAndTargetAreLegalAndPerformIfSo(Ability.Ether, tempHeroList[0]);

        tempHeroList = Evaluation.FindStandingHeroesWithManaLessThanAmount(0.3f, TeamHeroCoder.BattleState.allyHeroes);
        tempHeroList = Evaluation.FindHeroesWithCharacterClass(HeroJobClass.Wizard, tempHeroList);
        if (tempHeroList.Count > 0)
            CheckIfAbilityAndTargetAreLegalAndPerformIfSo(Ability.Ether, tempHeroList[0]);

        #endregion

        #region Silence Rememdy Usage

        tempHeroList = Evaluation.FindHeroesWithStatusEffect(StatusEffect.Silence, TeamHeroCoder.BattleState.allyHeroes);
        tempHeroList = Evaluation.FindHeroesWithCharacterClass(HeroJobClass.Cleric, tempHeroList);
        if (tempHeroList.Count > 0)
            CheckIfAbilityAndTargetAreLegalAndPerformIfSo(Ability.SilenceRemedy, tempHeroList[0]);

        tempHeroList = Evaluation.FindHeroesWithStatusEffect(StatusEffect.Silence, TeamHeroCoder.BattleState.allyHeroes);
        tempHeroList = Evaluation.FindHeroesWithCharacterClass(HeroJobClass.Wizard, tempHeroList);
        if (tempHeroList.Count > 0)
            CheckIfAbilityAndTargetAreLegalAndPerformIfSo(Ability.SilenceRemedy, tempHeroList[0]);

        #endregion

        #region Positive Status Effects

        tempHeroList = Evaluation.FindHeroesWithoutStatusEffect(StatusEffect.Faith, TeamHeroCoder.BattleState.allyHeroes);
        tempHeroList = Evaluation.FindHeroesWithCharacterClass(HeroJobClass.Wizard, tempHeroList);
        if (tempHeroList.Count > 0)
            CheckIfAbilityAndTargetAreLegalAndPerformIfSo(Ability.Faith, tempHeroList[0]);

        tempHeroList = Evaluation.FindHeroesWithoutStatusEffect(StatusEffect.Brave, TeamHeroCoder.BattleState.allyHeroes);
        tempHeroList = Evaluation.FindHeroesWithCharacterClass(HeroJobClass.Fighter, tempHeroList);
        if (tempHeroList.Count > 0)
            CheckIfAbilityAndTargetAreLegalAndPerformIfSo(Ability.Brave, tempHeroList[0]);

        tempHeroList = Evaluation.FindHeroesWithoutStatusEffect(StatusEffect.Faith, TeamHeroCoder.BattleState.allyHeroes);
        tempHeroList = Evaluation.FindHeroesWithCharacterClass(HeroJobClass.Cleric, tempHeroList);
        if (tempHeroList.Count > 0)
            CheckIfAbilityAndTargetAreLegalAndPerformIfSo(Ability.Faith, tempHeroList[0]);

        tempHeroList = Evaluation.FindHeroesWithoutStatusEffect(StatusEffect.Haste, TeamHeroCoder.BattleState.allyHeroes);
        if (tempHeroList.Count > 0)
            CheckIfAbilityAndTargetAreLegalAndPerformIfSo(Ability.Haste, tempHeroList[0]);

        #endregion

        #region Low Importance Heals

        if (allyHeroesWithHealthLessThan70Percent.Count > 0)
            CheckIfAbilityAndTargetAreLegalAndPerformIfSo(Ability.QuickHeal, allyHeroesWithHealthLessThan70Percent[0]);

        if (allyHeroesWithHealthLessThan70Percent.Count > 0)
            CheckIfAbilityAndTargetAreLegalAndPerformIfSo(Ability.CureLight, allyHeroesWithHealthLessThan70Percent[0]);

        #endregion

    }

    static private void ProcessWizardAI()
    {
        Console.WriteLine("Processing AI for wizard");

        Hero target;
        int targetCount;
        List<Hero> tempHeroList;
        Hero wizardWithInitiative = TeamHeroCoder.BattleState.heroWithInitiative;

        if (Evaluation.HeroHasStatusEffect(StatusEffect.Faith, wizardWithInitiative))
        {
            targetCount = Evaluation.CountNumberOfStandingHeroes(TeamHeroCoder.BattleState.foeHeroes);
            if (targetCount > 2)
                CheckIfAbilityAndTargetAreLegalAndPerformIfSo(Ability.Meteor);
        }

        #region Quick Dispel

        tempHeroList = Evaluation.FindHeroesWithPositiveStatusEffectsMoreThanAmount(1, TeamHeroCoder.BattleState.foeHeroes);
        if (tempHeroList.Count > 0)
            CheckIfAbilityAndTargetAreLegalAndPerformIfSo(Ability.QuickDispel, tempHeroList[0]);

        tempHeroList = Evaluation.FindHeroesWithStatusEffect(StatusEffect.Haste, TeamHeroCoder.BattleState.foeHeroes);
        if (tempHeroList.Count > 0)
            CheckIfAbilityAndTargetAreLegalAndPerformIfSo(Ability.QuickDispel, tempHeroList[0]);

        tempHeroList = Evaluation.FindHeroesWithStatusEffect(StatusEffect.AutoLife, TeamHeroCoder.BattleState.foeHeroes);
        if (tempHeroList.Count > 0)
            CheckIfAbilityAndTargetAreLegalAndPerformIfSo(Ability.QuickDispel, tempHeroList[0]);

        tempHeroList = Evaluation.FindHeroesWithStatusEffect(StatusEffect.Faith, TeamHeroCoder.BattleState.foeHeroes);
        tempHeroList = Evaluation.FindHeroesWithCharacterClass(HeroJobClass.Wizard, tempHeroList);
        if (tempHeroList.Count > 0)
            CheckIfAbilityAndTargetAreLegalAndPerformIfSo(Ability.QuickDispel, tempHeroList[0]);

        tempHeroList = Evaluation.FindHeroesWithStatusEffect(StatusEffect.Faith, TeamHeroCoder.BattleState.foeHeroes);
        tempHeroList = Evaluation.FindHeroesWithCharacterClass(HeroJobClass.Cleric, tempHeroList);
        if (tempHeroList.Count > 0)
            CheckIfAbilityAndTargetAreLegalAndPerformIfSo(Ability.QuickDispel, tempHeroList[0]);

        tempHeroList = Evaluation.FindHeroesWithStatusEffect(StatusEffect.Brave, TeamHeroCoder.BattleState.foeHeroes);
        tempHeroList = Evaluation.FindHeroesWithCharacterClass(HeroJobClass.Fighter, tempHeroList);
        if (tempHeroList.Count > 0)
            CheckIfAbilityAndTargetAreLegalAndPerformIfSo(Ability.QuickDispel, tempHeroList[0]);

        tempHeroList = Evaluation.FindHeroesWithStatusEffect(StatusEffect.Brave, TeamHeroCoder.BattleState.foeHeroes);
        tempHeroList = Evaluation.FindHeroesWithCharacterClass(HeroJobClass.Monk, tempHeroList);
        if (tempHeroList.Count > 0)
            CheckIfAbilityAndTargetAreLegalAndPerformIfSo(Ability.QuickDispel, tempHeroList[0]);

        #endregion

        #region Ether Usage

        tempHeroList = Evaluation.FindStandingHeroesWithManaLessThanAmount(0.3f, TeamHeroCoder.BattleState.allyHeroes);
        tempHeroList = Evaluation.FindHeroesWithCharacterClass(HeroJobClass.Cleric, tempHeroList);
        if (tempHeroList.Count > 0)
            CheckIfAbilityAndTargetAreLegalAndPerformIfSo(Ability.Ether, tempHeroList[0]);

        tempHeroList = Evaluation.FindStandingHeroesWithManaLessThanAmount(0.3f, TeamHeroCoder.BattleState.allyHeroes);
        tempHeroList = Evaluation.FindHeroesWithCharacterClass(HeroJobClass.Wizard, tempHeroList);
        if (tempHeroList.Count > 0)
            CheckIfAbilityAndTargetAreLegalAndPerformIfSo(Ability.Ether, tempHeroList[0]);

        #endregion

        #region Silence Rememdy Usage

        tempHeroList = Evaluation.FindHeroesWithStatusEffect(StatusEffect.Silence, TeamHeroCoder.BattleState.allyHeroes);
        tempHeroList = Evaluation.FindHeroesWithCharacterClass(HeroJobClass.Cleric, tempHeroList);
        if (tempHeroList.Count > 0)
            CheckIfAbilityAndTargetAreLegalAndPerformIfSo(Ability.SilenceRemedy, tempHeroList[0]);

        tempHeroList = Evaluation.FindHeroesWithStatusEffect(StatusEffect.Silence, TeamHeroCoder.BattleState.allyHeroes);
        tempHeroList = Evaluation.FindHeroesWithCharacterClass(HeroJobClass.Wizard, tempHeroList);
        if (tempHeroList.Count > 0)
            CheckIfAbilityAndTargetAreLegalAndPerformIfSo(Ability.SilenceRemedy, tempHeroList[0]);

        #endregion

        targetCount = Evaluation.CountNumberOfStandingHeroes(TeamHeroCoder.BattleState.foeHeroes);
        if (targetCount > 2)
            CheckIfAbilityAndTargetAreLegalAndPerformIfSo(Ability.Fireball);

        target = Evaluation.FindStandingHeroWithLowestHealth(TeamHeroCoder.BattleState.foeHeroes);
        CheckIfAbilityAndTargetAreLegalAndPerformIfSo(Ability.FlameStrike, target);

        target = Evaluation.FindStandingHeroWithLowestHealth(TeamHeroCoder.BattleState.foeHeroes);
        CheckIfAbilityAndTargetAreLegalAndPerformIfSo(Ability.MagicMissile, target);
    }

}

